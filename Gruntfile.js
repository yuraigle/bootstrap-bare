module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      options: {
        mangle: false,
        compress: false
      },
      build: {
        src: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/bootstrap/dist/js/bootstrap.min.js',
          'src/scripts/main.js'
        ],
        dest: 'build/script.min.js'
      }
    },

    less: {
      dist: {
        options: {
          cleancss: true,
          modifyVars: {}
        },
        files: {
          "build/style.min.css": "src/styles/main.less"
        }
      }
    },

    processhtml: {
      dist: {
        options: {
          process: true,
          data: {}
        },
        files: {
          'tmp/pages/index.html': ['src/pages/index.html']
        }
      },
    },

    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        expand: true,
        cwd: 'tmp/pages/',
        src: [
          '*.html',
          '**/*.html'
        ],
        dest: 'build/'
      }
    },

    copy: {
      main: {
        files: [
          // include my custom images from src/images/
          {expand: true, flatten: true, cwd: 'src/images/', src: ['**'], dest: 'build/img/', filter: 'isFile'},

          // include any files from src/files/ such as .htaccess, robots.txt etc
          {expand: true, flatten: true, cwd: 'src/files/', src: ['**'], dest: 'build/', filter: 'isFile', dot: true},

          // include framework fonts and files
          {expand: true, flatten: true, cwd: 'bower_components/bootstrap/dist/fonts', src: ['**'], dest: 'build/fonts', filter: 'isFile', dot: true},
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('default', ['processhtml', 'htmlmin', 'uglify', 'less', 'copy']);

};